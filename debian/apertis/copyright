Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2006, 2007, Sun Microsystems, Inc.
License: BSD-3-clause

Files: debian/*
Copyright: 2003-2005, Nicolas Sabouret <nico@debian.org>
           2007-2008, Paul Cager <paul-debian@home.paulcager.org>
           2008-2009, Michael Koch <konqueror@gmx.de>
           2010, Torsten Werner <twerner@debian.org>
           2013-2016, Emmanuel Bourg <ebourg@apache.org>
License: BSD-3-clause

Files: debian/bin/javacc.1
Copyright: 2003-2005, Nicolas Sabouret <nico@debian.org>
           2007-2008, Paul Cager <paul-debian@home.paulcager.org>
           2008-2009, Michael Koch <konqueror@gmx.de>
           2010, Torsten Werner <twerner@debian.org>
           2013-2016, Emmanuel Bourg <ebourg@apache.org>
License: GFDL-1.1+

Files: debian/control
Copyright: 2003-2005, Nicolas Sabouret <nico@debian.org>
           2007-2008, Paul Cager <paul-debian@home.paulcager.org>
           2008-2009, Michael Koch <konqueror@gmx.de>
           2010, Torsten Werner <twerner@debian.org>
           2013-2016, Emmanuel Bourg <ebourg@apache.org>
License: SISSL

Files: docs/grammars/*
Copyright: 2004, 2005, The Dojo Foundation
License: AFL-2.1

Files: docs/grammars/AsnParser.jj
Copyright: no-info-found
License: GPL-2+

Files: docs/grammars/ChemNumber.jj
Copyright: no-info-found
License: LGPL

Files: docs/grammars/JSONParser.jjt
Copyright: 2011-2013, Mike Norman
License: ISC

Files: docs/grammars/RTFParser.jj
Copyright: 2001, eTranslate, Inc.
License: LGPL-2.1+

Files: examples/Interpreter/MyNode.java
Copyright: 2006, Sreenivasa Viswanadha <sreeni@viswanadha.net>
License: BSD-3-clause

Files: examples/JavaGrammars/cpp/*
Copyright: 2011, 2012, Google Inc.
 2006, Sun Microsystems, Inc.
License: BSD-3-clause

Files: grammars/*
Copyright: 2004, 2005, The Dojo Foundation
License: AFL-2.1

Files: grammars/AsnParser.jj
Copyright: no-info-found
License: GPL-2+

Files: grammars/ChemNumber.jj
Copyright: no-info-found
License: LGPL

Files: grammars/JSONParser.jjt
Copyright: 2011-2013, Mike Norman
License: ISC

Files: grammars/RTFParser.jj
Copyright: 2001, eTranslate, Inc.
License: LGPL-2.1+

Files: scripts/*
Copyright: 2006, Andrew Tomazos <andrew@tomazos.com>
License: GPL-2+

Files: src/*
Copyright: 2006, Tim Pizey
License: BSD-3-clause

Files: src/main/*
Copyright: 2006, 2007, Sun Microsystems, Inc.
License: BSD-3-clause

Files: src/main/java/org/javacc/Version.java
Copyright: 2011, 2012, Google Inc.
 2006, Sun Microsystems, Inc.
License: BSD-3-clause

Files: src/main/java/org/javacc/jjdoc/JJDocOptions.java
Copyright: 2005, 2006, Kees Jan Koster kjkoster@kjkoster.org
License: BSD-3-clause

Files: src/main/java/org/javacc/jjtree/ASTGrammar.java
 src/main/java/org/javacc/jjtree/ASTNodeDescriptor.java
 src/main/java/org/javacc/jjtree/CPPNodeFiles.java
 src/main/java/org/javacc/jjtree/JJTree.java
 src/main/java/org/javacc/jjtree/NodeFiles.java
Copyright: 2011, 2012, Google Inc.
 2006, Sun Microsystems, Inc.
License: BSD-3-clause

Files: src/main/java/org/javacc/jjtree/JJTreeOptions.java
Copyright: 2011, Google Inc.
 2005, 2006, Kees Jan Koster kjkoster@kjkoster.org
License: BSD-3-clause

Files: src/main/java/org/javacc/parser/LexGen.java
 src/main/java/org/javacc/parser/LexGenCPP.java
 src/main/java/org/javacc/parser/Main.java
 src/main/java/org/javacc/parser/NfaState.java
 src/main/java/org/javacc/parser/Options.java
 src/main/java/org/javacc/parser/OtherFilesGen.java
 src/main/java/org/javacc/parser/OtherFilesGenCPP.java
 src/main/java/org/javacc/parser/ParseEngine.java
 src/main/java/org/javacc/parser/ParseGen.java
 src/main/java/org/javacc/parser/RStringLiteral.java
Copyright: 2011, 2012, Google Inc.
 2006, Sun Microsystems, Inc.
License: BSD-3-clause

Files: src/main/java/org/javacc/parser/OutputFile.java
Copyright: 2007-2009, Paul Cager.
License: BSD-2-clause

Files: src/main/java/org/javacc/utils/*
Copyright: 2007-2009, Paul Cager.
License: BSD-2-clause

Files: src/main/javacc/*
Copyright: 2007-2009, Paul Cager.
License: BSD-2-clause

Files: src/main/javacc/JavaCC.jj
Copyright: 2011, 2012, Google Inc.
 2006, Sun Microsystems, Inc.
License: BSD-3-clause

Files: src/test/java/org/javacc/parser/*
Copyright: 2006, 2007, Sun Microsystems, Inc.
License: BSD-3-clause

Files: test/*
Copyright: 2007, 2008, Paul Cager
License: BSD-2-clause

Files: test/build.xml
Copyright: 2006, 2007, Sun Microsystems, Inc.
License: BSD-3-clause

Files: test/gwtTemplate/*
Copyright: 2006, 2007, Sun Microsystems, Inc.
License: BSD-3-clause

Files: test/gwtUnicodeTemplate/*
Copyright: 2006, 2007, Sun Microsystems, Inc.
License: BSD-3-clause

Files: test/java7features/*
Copyright: 2006, 2007, Sun Microsystems, Inc.
License: BSD-3-clause

Files: test/lineNumbers/*
Copyright: 2006, 2007, Sun Microsystems, Inc.
License: BSD-3-clause

Files: test/lom/build.xml
Copyright: 2007-2009, Paul Cager.
License: BSD-2-clause

Files: test/newToken/build.xml
Copyright: 2007-2009, Paul Cager.
License: BSD-2-clause

Files: bootstrap/* docs/faq.md docs/grammars/CPPParser.jj docs/grammars/infosapient.jj grammars/CPPParser.jj grammars/infosapient.jj keystore src/main/java/org/javacc/jjtree/CPPCodeGenerator.java src/main/java/org/javacc/jjtree/CPPJJTreeState.java src/main/java/org/javacc/jjtree/JavaCodeGenerator.java src/main/java/org/javacc/jjtree/CPPCodeGenerator.java src/main/java/org/javacc/jjtree/CPPJJTreeState.java src/main/java/org/javacc/jjtree/JavaCodeGenerator.java src/main/java/org/javacc/jjtree/CPPCodeGenerator.java src/main/java/org/javacc/jjtree/CPPJJTreeState.java src/main/java/org/javacc/jjtree/JavaCodeGenerator.java src/main/java/org/javacc/parser/CPPFiles.java src/main/java/org/javacc/parser/CodeGenerator.java src/main/java/org/javacc/parser/ParseGenCPP.java src/main/java/org/javacc/parser/CPPFiles.java src/main/java/org/javacc/parser/CodeGenerator.java src/main/java/org/javacc/parser/ParseGenCPP.java src/main/java/org/javacc/parser/CPPFiles.java src/main/java/org/javacc/parser/CodeGenerator.java src/main/java/org/javacc/parser/ParseGenCPP.java
Copyright: 2005-2006, Kees Jan Koster kjkoster@kjkoster.org
 2006, Sreenivasa Viswanadha <sreeni@viswanadha.net>
 2006-2007, Sun Microsystems, Inc.
 2006, Tim Pizey
 2007, Paul Cager
License: BSD-3-clause
